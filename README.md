# Ontologie de cinémathèque

## À propos

L'ontologie de cinémathèque fournit les concepts et propriétés principaux pour décrire les données d'une cinémathèque dans le Web Sémantique.

## Contribution

Le développement de l'ontologie se fait à l'aide de l'outil graphique [Eddy](https://github.com/obdasystems/eddy) qui utilise le formalisme [Graphol](http://www.obdasystems.com/graphol).
Pour l'utilisez, exécutez à partir de la racine de ce répertoire:

```
$ nix-shell
$ eddy cmtqo/cmtqo.graphol
```

Après avoir modifié l'ontologie, vous devrez modifier les fichiers du dossier `ontology`.
Pour se faire:

1. À partir de l'interface d'Eddy, exportez l'ontologie au format [Turtle/RDF](https://fr.wikipedia.org/wiki/Turtle_(syntaxe)) vers le dossier `cmtqo`
2. Tout à partir d'Eddy, exportez le/les diagrammes de l'ontologie au format `jpg` vers le dossier `cmtqo`
3. S'il y a lieu, modifiez le fichier `config/metadata.yml` avec des nouvelles valeurs
4. Exécutez la commande `make run-cmtqo-exe`.
5. Les changements peut être soumis avec Git.
