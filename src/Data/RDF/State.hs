{-# LANGUAGE OverloadedStrings #-}

module Data.RDF.State where

import Data.RDF (RDF)
import qualified Data.RDF as RDF
import Control.Monad.State
-- import Data.Maybe

-- newtype RdfState rdfImpl m a = RdfState { unRdfState :: StateT (RDF rdfImpl) m a }
--   deriving (Functor, Applicative, Monad, MonadState (RDF rdfImpl))
type RdfState rdfImpl m a = StateT (RDF rdfImpl) m a

baseUrl :: (RDF.Rdf rdfImpl, Monad m)
        => RdfState rdfImpl m (Maybe RDF.BaseUrl)
baseUrl = do
  graph <- get
  return $ RDF.baseUrl graph

mkRdf :: (RDF.Rdf rdfImpl, Monad m)
      => RDF.Triples
      -> Maybe RDF.BaseUrl
      -> RDF.PrefixMappings
      -> RdfState rdfImpl m ()
mkRdf triples baseUrlMaybe mappings =
  put $ RDF.mkRdf triples baseUrlMaybe mappings

addTriple :: (RDF.Rdf rdfImpl, Monad m)
          => RDF.Triple
          -> RdfState rdfImpl m ()
addTriple triple = do
  graph <- get
  put $ RDF.addTriple graph triple

removeTriple :: (RDF.Rdf rdfImpl, Monad m)
             => RDF.Triple
             -> RdfState rdfImpl m ()
removeTriple triple = do
  graph <- get
  put $ RDF.removeTriple graph triple

triplesOf :: (RDF.Rdf rdfImpl, Monad m)
          => RdfState rdfImpl m RDF.Triples
triplesOf = do
  fmap RDF.triplesOf get

uniqTriplesOf :: (RDF.Rdf rdfImpl, Monad m)
          => RdfState rdfImpl m RDF.Triples
uniqTriplesOf = do
  fmap RDF.uniqTriplesOf get

prefixMappings :: (RDF.Rdf rdfImpl, Monad m)
               => RdfState rdfImpl m RDF.PrefixMappings
prefixMappings = do
  graph <- get
  return $ RDF.prefixMappings graph

addPrefixMappings :: (RDF.Rdf rdfImpl, Monad m)
                  => RDF.PrefixMappings
                  -> Bool
                  -> RdfState rdfImpl m ()
addPrefixMappings mappings replaceOldMapping = do
  graph <- get
  put $ RDF.addPrefixMappings graph mappings replaceOldMapping

empty :: (RDF.Rdf rdfImpl, Monad m)
      => RdfState rdfImpl m ()
empty = put $ RDF.empty

query :: (RDF.Rdf rdfImpl, Monad m)
      => Maybe RDF.Node
      -> Maybe RDF.Node
      -> Maybe RDF.Node
      -> RdfState rdfImpl m RDF.Triples
query s p o = do
  graph <- get
  return $ RDF.query graph s p o

select :: (RDF.Rdf rdfImpl, Monad m)
       => RDF.NodeSelector
       -> RDF.NodeSelector
       -> RDF.NodeSelector
       -> RdfState rdfImpl m RDF.Triples
select s p o = do
  graph <- get
  return $ RDF.select graph s p o
