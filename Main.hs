{-# LANGUAGE OverloadedStrings #-}

module Main where

import System.Environment (getArgs)
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import Data.Maybe (fromMaybe, listToMaybe)
import Data.RDF (RDF)
import qualified Data.RDF as RDF
import qualified Data.RDF.Namespace as RDF
import Data.RDF.State
import Control.Monad (forM_)
import Control.Monad.IO.Class (MonadIO)
import Control.Monad.State (execStateT)
import System.IO (withFile, IOMode(..))
import Data.Yaml (FromJSON(..), ParseException, decodeFileEither, (.:), withObject)
import qualified Data.Map as Map
import System.FilePath (dropExtension)

-- Semantic versioning X.Y.Z
data SemVer = SemVer { major :: Int
                     , minor :: Int
                     , patch :: Maybe Int
                     }

instance FromJSON SemVer where
  parseJSON = withObject "SemVer" $ \v -> SemVer
    <$> v .: "major"
    <*> v .: "minor"
    <*> v .: "patch"

instance Show SemVer where
  show s = (show $ major s)
        <> "."
        <> (show $ minor s)
        <> (fromMaybe "" $ fmap ("." <>) $ fmap show $ patch s)

data OntologyMetadata = OntologyMetadata { version :: SemVer
                                         , label :: Text
                                         , comment :: Text
                                         , publishers :: [Text]
                                         , license :: Text
                                         , rights :: Text
                                         , repository :: Text
                                         } deriving (Show)

instance FromJSON OntologyMetadata where
  parseJSON = withObject "OntologyMetadata" $ \v -> OntologyMetadata
      <$> v .: "version"
      <*> v .: "label"
      <*> v .: "comment"
      <*> v .: "publishers"
      <*> v .: "license"
      <*> v .: "rights"
      <*> v .: "repository"

data Options = Options { optionsOntologyFile :: !FilePath
                       , optionsMetadataFile :: !FilePath
                       }

dcterms :: RDF.Namespace
dcterms = RDF.mkPrefixedNS' "dcterms" "http://purl.org/dc/terms/"

sd :: RDF.Namespace
sd = RDF.mkPrefixedNS' "sd" "https://w3id.org/okn/o/sd#"

rdftype :: RDF.Node
rdftype = RDF.unode $ RDF.mkUri RDF.rdf "type"

owlOntology :: RDF.Node
owlOntology = RDF.unode $ RDF.mkUri RDF.owl "Ontology"

owlversionInfo :: RDF.Node
owlversionInfo = RDF.unode $ RDF.mkUri RDF.owl "versionInfo"

owlversionIRI :: RDF.Node
owlversionIRI = RDF.unode $ RDF.mkUri RDF.owl "versionIRI"

rdfslabel :: RDF.Node
rdfslabel = RDF.unode $ RDF.mkUri RDF.rdfs "label"

rdfscomment :: RDF.Node
rdfscomment = RDF.unode $ RDF.mkUri RDF.rdfs "comment"

dctermslicense :: RDF.Node
dctermslicense = RDF.unode $ RDF.mkUri dcterms "license"

dctermsrights :: RDF.Node
dctermsrights = RDF.unode $ RDF.mkUri dcterms "rights"

dctermspublisher :: RDF.Node
dctermspublisher = RDF.unode $ RDF.mkUri dcterms "publisher"

sdcodeRepository :: RDF.Node
sdcodeRepository = RDF.unode $ RDF.mkUri sd "codeRepository"

main :: IO ()
main = do
  args <- getArgs

  case args of
    (ontologyFile:metadataFile:[]) -> do
      run $ Options ontologyFile metadataFile
    _ -> do
      error $ "[ERROR] Arguments missing. Usage: cmtqo <ontologyfile> <metadatafile>"

run :: Options -> IO ()
run options = do
  let ontologyFile = optionsOntologyFile options
  let metadataFile = optionsMetadataFile options

  putStrLn $ "[INFO] Reading ontology metadata from file " <> metadataFile
  metadataE <- readMetadataFile metadataFile
  metadata <- either (error . show) return metadataE

  putStrLn $ "[INFO] Reading ontology file " <> ontologyFile
  let ontoBaseUrl = RDF.BaseUrl $ "http://data.cinematheque.qc.ca/ontology/cmtqo"
  owlGraphE <- readOntologyFile ontologyFile ontoBaseUrl
  owlGraph <- either (error . show) (return) owlGraphE
  putStrLn $ "[INFO] No syntax errors in file."

  -- Create new prefix mappings
  let newPrefixMappings = RDF.ns_mappings [ dcterms, sd ]
  let pmappings = (removePrefixMapping "cmtqo" $ RDF.prefixMappings owlGraph) <> newPrefixMappings

  -- Reload ontology with new prefixes and automaticly resolve URIs
  let onto = RDF.mkRdf (RDF.uniqTriplesOf owlGraph) Nothing pmappings :: RDF RDF.TList

  -- Add new metadata in ontology
  newOntology <- execStateT (removeOldMetadata ontoBaseUrl >> createOntologyMetadata ontoBaseUrl metadata) onto

  -- Write ontology to new file
  let ontologyTtlFile = dropExtension ontologyFile <> ".ttl"
  putStrLn $ "[INFO] Writing new ontology to " <> ontologyTtlFile
  withFile ontologyTtlFile WriteMode (\h -> RDF.hWriteRdf (RDF.TurtleSerializer (Just $ RDF.unBaseUrl ontoBaseUrl) pmappings) h newOntology)

  -- Replace absolute URIs containing the base uri to relative URIs
  Text.readFile ontologyTtlFile >>= \txt -> Text.writeFile ontologyTtlFile (Text.replace (RDF.unBaseUrl ontoBaseUrl <> "#") "#" txt)
  newOwlGraphE <- readOntologyFile ontologyTtlFile ontoBaseUrl
  _ <- either (error . show) (return) newOwlGraphE
  putStrLn $ "[INFO] No syntax errors in new file."

readMetadataFile :: FilePath
                 -> IO (Either ParseException OntologyMetadata)
readMetadataFile metadataFile = decodeFileEither metadataFile

readOntologyFile :: FilePath
                 -> RDF.BaseUrl
                 -> IO (Either RDF.ParseFailure (RDF RDF.TList))
readOntologyFile ontologyFile ontoBaseUrl = RDF.parseFile turtleParser ontologyFile
  where turtleParser = RDF.TurtleParser (Just ontoBaseUrl) Nothing

createOntologyMetadata :: (RDF.Rdf rdfImpl, MonadIO m)
                       => RDF.BaseUrl
                       -> OntologyMetadata
                       -> RdfState rdfImpl m ()
createOntologyMetadata ontoUri ontoMetadata = do
  let ontoNode = (RDF.unode $ RDF.unBaseUrl ontoUri)

  addTriple $ RDF.triple ontoNode rdftype owlOntology

  let versionInfo = Text.pack $ show $ version ontoMetadata
  addTriple $ RDF.triple ontoNode owlversionInfo (RDF.lnode $ RDF.PlainL $ versionInfo)
  addTriple $ RDF.triple ontoNode owlversionIRI (RDF.unode $ "/" <> versionInfo)
  addTriple $ RDF.triple ontoNode rdfslabel (RDF.lnode $ RDF.PlainL $ label ontoMetadata)
  addTriple $ RDF.triple ontoNode rdfscomment (RDF.lnode $ RDF.PlainL $ comment ontoMetadata)
  addTriple $ RDF.triple ontoNode dctermslicense (RDF.lnode $ RDF.PlainL $ license ontoMetadata)
  addTriple $ RDF.triple ontoNode dctermsrights (RDF.lnode $ RDF.PlainL $ rights ontoMetadata)
  addTriple $ RDF.triple ontoNode sdcodeRepository (RDF.lnode $ RDF.PlainL $ repository ontoMetadata)
  forM_ (publishers ontoMetadata) $ \publisher -> do
    addTriple $ RDF.triple ontoNode dctermspublisher (RDF.lnode $ RDF.PlainL $ publisher)

  --- Include priorVersion, created, modified
  return ()

baseUriSelector :: RDF.BaseUrl -> RDF.Node -> Bool
baseUriSelector b (RDF.UNode n) = Text.isPrefixOf (RDF.unBaseUrl b) n
baseUriSelector _ _ = False

removeOldMetadata :: (RDF.Rdf rdfImpl, MonadIO m)
                  => RDF.BaseUrl
                  -> RdfState rdfImpl m ()
removeOldMetadata b = do
  ontoTriples <- query (Just $ RDF.unode $ RDF.unBaseUrl b)
                       Nothing
                       Nothing
  mapM_ removeTriple ontoTriples

getVersionInfo :: (RDF.Rdf rdfImpl, Monad m)
               => RDF.BaseUrl
               -> RdfState rdfImpl m (Maybe Text)
getVersionInfo ontoUri = do
  versionIRITriples <- query (Just $ RDF.unode $ RDF.unBaseUrl ontoUri)
                             (Just $ RDF.unode $ RDF.mkUri RDF.owl $ "versionIRI")
                             Nothing

  return $ getVersionInfoFromIRI versionIRITriples

  where
    getVersionInfoFromIRI :: [RDF.Triple] -> Maybe Text
    getVersionInfoFromIRI versionIRITriples = do
      RDF.Triple _ _ versionIRINode <- listToMaybe versionIRITriples
      versionIRI <- getUNodeURI versionIRINode
      return $ Text.reverse $ Text.takeWhile (/='/') $ Text.reverse versionIRI

getUNodeURI :: RDF.Node
            -> Maybe Text
getUNodeURI (RDF.UNode uri) = Just uri
getUNodeURI _ = Nothing

removePrefixMapping :: Text
                    -> RDF.PrefixMappings
                    -> RDF.PrefixMappings
removePrefixMapping prefix (RDF.PrefixMappings mappings) =
  RDF.PrefixMappings (Map.delete prefix mappings)
