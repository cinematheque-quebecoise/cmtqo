#!/usr/bin/env nix-shell
#!nix-shell -i runhaskell

{-# LANGUAGE OverloadedStrings #-}

import Data.RDF (RDF)
import qualified Data.RDF as RDF
import System.Environment (getArgs)
import Data.Maybe (listToMaybe)

main :: IO ()
main = do
  args <- getArgs
  let ontologyPathM = listToMaybe args

  case ontologyPathM of
    Nothing -> error $ "Missing first argument: Must be a file path to OWL file!"
    Just ontologyPath -> do
      let cmtqoUri = "http://data.cinematheque.qc.ca/ontology/cmtqo"
      let baseUrl = RDF.BaseUrl cmtqoUri
      let turtleParser = RDF.TurtleParser (Just baseUrl) Nothing
      owlGraphE <- RDF.parseFile turtleParser ontologyPath :: IO (Either RDF.ParseFailure (RDF RDF.TList))

      putStrLn $ "Validating file " <> ontologyPath <> " ..."
      case owlGraphE of
        Left (RDF.ParseFailure errMsg) -> error errMsg
        Right _ -> putStrLn "No syntax errors in file."
