import (builtins.fetchGit {
  # Descriptive name to make the store path easier to identify
  name = "nixos-unstable-2020-08-24";
  url = "https://github.com/nixos/nixpkgs-channels/";
  # Commit hash for nixos-unstable as of 2018-09-12
  # `git ls-remote https://github.com/nixos/nixpkgs-channels nixos-unstable`
  ref = "refs/heads/nixos-unstable";
  # nixos-unstable-2020-04-29
  # rev = "7c399a4ee080f33cc500a3fda33af6fccfd617bd";
  # nixos-unstable-2020-08-24
  rev = "c59ea8b8a0e7f927e7291c14ea6cd1bd3a16ff38";
}) {}
