let
  pkgs = import ./nixpkgs.nix;

  compiler = "ghc883";

  haskellPackages  = pkgs.haskell.packages.${compiler}.override {
    overrides = self: upser: rec {
      rdf4h = self.callCabal2nix "rdf4h" (builtins.fetchGit {
        url = "https://github.com/robstewart57/rdf4h.git";
        ref = "refs/heads/master";
        rev = "9658c9c361fb175f614a127c96da8ec42665334a";
      }) {};
    };
  };
in
  haskellPackages.callCabal2nix "cmtqo" (./.) {
    rdf4h = pkgs.haskell.lib.dontCheck haskellPackages.rdf4h;
  }
