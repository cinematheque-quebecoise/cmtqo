Markdown documentation created by [pyLODE](http://github.com/rdflib/pyLODE) 2.8.3

# Cinematheque ontology

## Metadata
* **URI**
  * `file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl`
* **Publisher(s)**
  * https://cinematheque.qc.ca
* **Version Information**
  * 0.1
* **License &amp; Rights**
  * [https://creativecommons.org/licenses/by/4.0](https://creativecommons.org/licenses/by/4.0)
  * (C) Cinematheque quebecoise 2020
* **Ontology RDF**
  * RDF ([cmtqo.ttl](turtle))
### Description
<p>Ontology to describe concepts and relations in a Cinematheque.</p>

## Table of Contents
1. [Classes](#classes)
1. [Object Properties](#objectproperties)
1. [Datatype Properties](#datatypeproperties)
1. [Namespaces](#namespaces)
1. [Legend](#legend)


## Overview

**Figure 1:** Ontology overview
## Classes
[E21_Person](#E21_Person),
[E39_Actor](#E39_Actor),
[E40_Legal_Body](#E40_Legal_Body),
[E52_Time-Span](#E52_Time-Span),
[E53_Place](#E53_Place),
[E55_Type](#E55_Type),
[E7_Activity](#E7_Activity),
[Episode](#Episode),
[F15_Complex_Work](#F15_Complex_Work),
[F1_Work](#F1_Work),
[F21_Recording_Work](#F21_Recording_Work),
[F27_Work_Conception](#F27_Work_Conception),
[F29_Recording_Event](#F29_Recording_Event),
[Language](#Language),
[Q1762059](#Q1762059),
[Q34770](#Q34770),
[Q483394](#Q483394),
[Recording_Role_Activity](#Recording_Role_Activity),
[Role](#Role),
[Thing](#Thing),
[Work_Public_Release](#Work_Public_Release),
### Episode
Property | Value
--- | ---
URI | `file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Episode`
Super-classes |[frbroo:F21_Recording_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F21_Recording_Work) (c)<br />
In domain of |[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#episode_number](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#episode_number) (dp)<br />
### Language
Property | Value
--- | ---
URI | `file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Language`
Description | <p>Controlled vocabulary of languages</p>
In range of |[wdt:P407](http://www.wikidata.org/prop/direct/P407) (op)<br />
### Recording_Role_Activity
Property | Value
--- | ---
URI | `file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Recording_Role_Activity`
Description | <p>Activity with a specific role that occurs during a work recording event. Ex. directing, photographing, interpretating, etc.</p>
Super-classes |[crm:E7_Activity](http://www.cidoc-crm.org/cidoc-crm/E7_Activity) (c)<br />
In domain of |[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#in_the_role_of](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#in_the_role_of) (op)<br />[crm:P14_carried_out_by](http://www.cidoc-crm.org/cidoc-crm/P14_carried_out_by) (op)<br />
In range of |[crm:P9_consists_of](http://www.cidoc-crm.org/cidoc-crm/P9_consists_of) (op)<br />
### Role
Property | Value
--- | ---
URI | `file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Role`
Description | <p>Controlled vocabulary of work recording roles.</p>
Super-classes |[crm:E55_Type](http://www.cidoc-crm.org/cidoc-crm/E55_Type) (c)<br />
In range of |[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#in_the_role_of](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#in_the_role_of) (op)<br />
### Work_Public_Release
Property | Value
--- | ---
URI | `file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Work_Public_Release`
Description | <p>Actity when a work is first released to the public.</p>
Super-classes |[crm:E7_Activity](http://www.cidoc-crm.org/cidoc-crm/E7_Activity) (c)<br />
In domain of |[crm:P7_took_place_at](http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at) (op)<br />[crm:P4_has_time-span](http://www.cidoc-crm.org/cidoc-crm/P4_has_time-span) (op)<br />
### F15_Complex_Work
Property | Value
--- | ---
URI | `http://iflastandards.info/ns/fr/frbr/frbroo/F15_Complex_Work`
Description | <p>This class comprises works that have other works as members. The members of a Complex Work may constitute alternatives to, derivatives of, or self-contained components of other members of the same Complex Work.</p> <p>In practice, no clear line can be drawn between parallel and subsequent processes in the evolution of a work. One part may not be finished when another is already revised. An initially monolithic work may be taken up and evolve in pieces. The member relationship of Work is based on the conceptual relationship, and should not be confused with the internal structural parts of an individual expression. The fact that an expression may contain parts from other work(s) does not make the expressed work complex. For instance, an anthology for which only one version exists is not a complex work.</p> <p>The boundaries of a Complex Work have nothing to do with the value of the intellectual achievement but only with the dominance of a concept. Thus, derivations such as translations are regarded as belonging to the same Complex Work, even though in addition they constitute an Individual Work themselves. In contrast, a Work that significantly takes up and merges concepts of other works so that it is no longer dominated by the initial concept is regarded as a new work. In cataloguing practice, detailed rules are established prescribing which kinds of derivation should be regarded as crossing the boundaries of a complex work. Adaptation and derivation graphs allow the recognition of distinct sub-units, i.e. a complex work contained in a larger complex work.</p> <p>As a Complex Work can be taken up by any creator who acquires the spirit of its concept, it is never finished in an absolute sense.</p> <p>Examples: - Work entitled ‘La Porte de l’Enfer’ by Auguste Rodin.</p> <ul> <li>Work entitled ‘Hamlet’ by William Shakespeare.</li> <li>Work entitled ‘Der Ring der Nibelungen’ by Richard Wagner.</li> <li>Work entitled ‘Carceri d’invenzione’ by Giovanni Battista Piranesi.</li> <li>Work entitled ‘Mass in B minor BWV 232’ by Johann Sebastian Bach.</li> </ul> <p>Properties: R10 has member (is member of): F1 Work</p>
In domain of |[frbroo:R10_has_member](http://iflastandards.info/ns/fr/frbr/frbroo/R10_has_member) (op)<br />
### F1_Work
Property | Value
--- | ---
URI | `http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work`
Description | <p>This class comprises distinct concepts or combinations of concepts identified in artistic and intellectual expressions, such as poems, stories or musical compositions. Such concepts may appear in the course of the coherent evolution of an original idea into one or more expressions that are dominated by the original idea. A Work may be elaborated by one or more Actors simultaneously or over time. The substance of Work is ideas. A Work may have members that are works in their own right.</p> <p>A Work can be either individual or complex. If it is individual its concept is completely realised in a single F22 Self-Contained Expression. If it is complex its concept is embedded in an F15 Complex Work. An F15 Complex Work consists of alternative members that are either F15 Complex Works themselves or F14 Individual Works. </p> <p>A Work is the product of an intellectual process of one or more persons, yet only indirect evidence about it is at our hands. This can be contextual information such as the existence of an order for a work, reflections of the creators themselves that are documented somewhere, and finally the expressions of the work created. As ideas normally take shape during discussion, elaboration and implementation, it is not reasonable to assume that a work starts with a complete concept. In some cases, it can be very difficult or impossible to define the whole of the concept of a work at a particular time. The objective evidence for such a notion can only be based on a stage of expressions at a given time. In this sense, the sets of ideas that constitute  particular self-contained expressions may be regarded as a kind of “snap-shot” of a work.</p> <p>A Work may include the concept of aggregating expressions of other works into a new expression. For instance, an anthology of poems is regarded as a work in its own right that makes use of expressions of the individual poems that have been selected and ordered as part of an intellectual process. This does not make the contents of the aggregated expressions part of this work, but only parts of the resulting expression.</p> <p>Examples:</p> <ul> <li> <p>Abstract content of Giovanni Battista Piranesi’s ‘Carcere XVI: the pier with chains: 1st state’ (F14).</p> </li> <li> <p>‘La Porte de l’Enfer’ by Auguste Rodin conceived between 1880 and 1917 (F15) ‘Hamlet’ by William Shakespeare (F15).</p> </li> </ul> <p>Properties: R1 is logical successor of (has successor): F1 Work R2 is derivative of (has derivative): F1 Work (R2.1 has type: E55 Type) R3 is realised in (realises): F22 Self-Contained Expression R40 has representative expression (is representative expression for): F22 Self-Contained Expression F2 Expression</p>
Sub-classes |[frbroo:F21_Recording_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F21_Recording_Work) (c)<br />
In domain of |[frbroo:R16i_was_initiated_by](http://iflastandards.info/ns/fr/frbr/frbroo/R16i_was_initiated_by) (op)<br />
In range of |[frbroo:R10_has_member](http://iflastandards.info/ns/fr/frbr/frbroo/R10_has_member) (op)<br />[frbroo:R2_is_derivative_of](http://iflastandards.info/ns/fr/frbr/frbroo/R2_is_derivative_of) (op)<br />
### F21_Recording_Work
Property | Value
--- | ---
URI | `http://iflastandards.info/ns/fr/frbr/frbroo/F21_Recording_Work`
Description | <p>Scope note:</p> <p>This class comprises works that conceptualise the capturing of features of perdurants. The characteristics of the manifestation of a recording work are those of the product of the capture process. The characteristics of any other works recorded are distinct from those of the recording work itself. In the case where the recorded perdurant expresses some Work, the respective instance of F21 is also an F16 Container Work.</p> <p>The concept of recording is not necessarily linked to the use of modern devices that allow for mechanical recording, such as tape recorders or cameras. However, in practice, library catalogues tend to regard as recordings only the products of such mechanical devices.</p> <p>But the concept of recording is very much linked to the notion that there is something that is recorded. In general, photographs or animated images are not to be regarded as instances of F21 Recording Work just because of the use of the medium, but simply as instances of F1 Work (or F15 Complex Work). Only such photographs and animated images that can be used as documentation are to be regarded as recordings.</p> <p>Examples:</p> <p>-The concept of recording the Swedish 17th century warship Vasa in August 1959 to April 1961. -The concept of documenting the Live Aid concerts July 13, 1985, London, Philadelphia, Sydney and Moscow. -The concept of making a photograph of the three Allied leaders at Yalta in February 1945. -Oceania Project’s concept of making a large digital acoustic data archive dedicated to East Australian humpback whale songs. -Concept of recording Louise Bourgeois’s activity in the documentary movie entitled ‘Louise Bourgeois: The Spider, the Mistress, and the Tangerine’.</p>
Super-classes |[frbroo:F1_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work) (c)<br />
Sub-classes |[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Episode](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Episode) (c)<br />
In domain of |[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#release_event](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#release_event) (op)<br />[dbo:budget](http://dbpedia.org/ontology/budget) (dp)<br />[frbroo:R2_is_derivative_of](http://iflastandards.info/ns/fr/frbr/frbroo/R2_is_derivative_of) (op)<br />[wdt:P136](http://www.wikidata.org/prop/direct/P136) (op)<br />[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#synopsis](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#synopsis) (dp)<br />[wdt:P407](http://www.wikidata.org/prop/direct/P407) (op)<br />
In range of |[frbroo:R22_created_a_realization_of](http://iflastandards.info/ns/fr/frbr/frbroo/R22_created_a_realization_of) (op)<br />
### F27_Work_Conception
Property | Value
--- | ---
URI | `http://iflastandards.info/ns/fr/frbr/frbroo/F27_Work_Conception`
Description | <p>This class comprises beginnings of evolutions of works. An instance of F27 Work Conception marks the initiation of the creation of a work. The work, as an intellectual construction, evolves from this point on, until the last known expression of it. The instance of E39 Actor with which a work is associated through the chain of properties F1 Work R16i was initiated by F27 Work Conception P14 carried out by E39 Actor corresponds to the notion of the “creator” of the work. In the case of commissioned works, it is not the commissioning that is regarded as the work conception, but the acceptance of the commission. This event does not always correlate with the date assigned in common library practice to the work, which is usually a later event (such as the date of completion of the first clean draft). In addition, F27 Work Conception can serve to document the circumstances that surrounded the appearance of the original idea for a work, when these are known.</p> <p>Examples:</p> <p>-Ludwig van Beethoven’s having the first ideas for his fifth symphony Pablo Picasso’s acceptance, in 1930, of Ambroise Vollard’s commission for a set of 100 etchings, now known as the ‘Vollard Suite’.</p> <p>-René Goscinny’s and Albert Uderzo’s first collaborative ideas for the comic book entitled ‘Asterix in Britain’ [comment: Goscinny wrote the script and Uderzo made the drawings; both are regarded as co-creators of that collaborative, at the same level of creative input, and no attempt is made to ascertain whether the ideas for the script preceded the ideas for the drawings, or vice-versa].</p> <p>-The combination of activities, carried out, among others, by Alfred Hitchcock, that began the process which eventually resulted in the movie entitled ‘Psycho’ coming into being.</p> <p>-Oscar Wilde’s having by May 1897 the initial idea of writing his poem entitled ‘The ballad of the Reading gaol’, inspired by his stay in the Reading prison from November 20, 1895 to May 18, 1897, and the execution of Charles Thomas Woolridge on July 7, 1896.</p>
In domain of |[crm:P14_carried_out_by](http://www.cidoc-crm.org/cidoc-crm/P14_carried_out_by) (op)<br />
In range of |[frbroo:R16i_was_initiated_by](http://iflastandards.info/ns/fr/frbr/frbroo/R16i_was_initiated_by) (op)<br />
### F29_Recording_Event
Property | Value
--- | ---
URI | `http://iflastandards.info/ns/fr/frbr/frbroo/F29_Recording_Event`
Description | <p>This class comprises activities that intend to convey (and preserve) the content of events in a recording, such as a live recording of a performance, a documentary, or other capture of a perdurant. Such activities may follow the directions of a recording plan. They may include postproduction.</p> <p>Examples:</p> <p>-The making of the recording of the third alternate take of the musical work titled ‘Blue Hawaii’ as performed by Elvis Presley in Hollywood, Calif., Radio Recorders, on March 22nd, 1961.</p> <p>-The making of the photograph of the three Allied leaders at Yalta in February 1945 The making of the recording of an East Australian humpback whale song in 1994 in the framework of the Oceania Project.</p> <p>-Filming Louise Bourgeois at work in the context of the shooting of the documentary movie entitled ‘Louise Bourgeois: The Spider, the Mistress, and the Tangerine’.</p>
In domain of |[frbroo:R22_created_a_realization_of](http://iflastandards.info/ns/fr/frbr/frbroo/R22_created_a_realization_of) (op)<br />[crm:P4_has_time-span](http://www.cidoc-crm.org/cidoc-crm/P4_has_time-span) (op)<br />[crm:P9_consists_of](http://www.cidoc-crm.org/cidoc-crm/P9_consists_of) (op)<br />[crm:P7_took_place_at](http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at) (op)<br />
### E21_Person
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/E21_Person`
Description | <p>This class comprises real persons who live or are assumed to have lived. Legendary figures that may have existed, such as Ulysses and King Arthur, fall into this class if the documentation refers to them as historical figures. In cases where doubt exists as to whether several persons are in fact identical, multiple instances can be created and linked to indicate their relationship. The CRM does not propose a specific form to support reasoning about possible identity.</p> <p>Examples: Tut-Ankh-Amun, Nelson Mandela</p>
Super-classes |[crm:E39_Actor](http://www.cidoc-crm.org/cidoc-crm/E39_Actor) (c)<br />
In domain of |[foaf:givenName](http://xmlns.com/foaf/0.1/givenName) (dp)<br />[foaf:name](http://xmlns.com/foaf/0.1/name) (dp)<br />[foaf:familyName](http://xmlns.com/foaf/0.1/familyName) (dp)<br />
### E39_Actor
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/E39_Actor`
Description | <p>This class comprises people, either individually or in groups, who have the potential to perform intentional actions of kinds for which someone may be held responsible.</p> <p>The CRM does not attempt to model the inadvertent actions of such actors. Individual people should be documented as instances of E21 Person, whereas groups should be documented as instances of either E74 Group or its subclass E40 Legal Body.</p> <p>Examples: London and Continental Railways (E40) the Governor of the Bank of England in 1975 (E21) Sir Ian McKellan (E21)</p>
Sub-classes |[crm:E40_Legal_Body](http://www.cidoc-crm.org/cidoc-crm/E40_Legal_Body) (c)<br />[crm:E21_Person](http://www.cidoc-crm.org/cidoc-crm/E21_Person) (c)<br />
In range of |[crm:P14_carried_out_by](http://www.cidoc-crm.org/cidoc-crm/P14_carried_out_by) (op)<br />
### E40_Legal_Body
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/E40_Legal_Body`
Description | <p>This class comprises institutions or groups of people that have obtained a legal recognition as a group and can act collectively as agents.</p> <p>This means that they can perform actions, own property, create or destroy things and can be held collectively responsible for their actions like individual people. The term 'personne morale' is often used for this in French. </p> <p>Examples: Greenpeace Paveprime Ltd the National Museum of Denmark</p>
Super-classes |[crm:E39_Actor](http://www.cidoc-crm.org/cidoc-crm/E39_Actor) (c)<br />
Sub-classes |[wd:Q1762059](http://www.wikidata.org/entity/Q1762059) (c)<br />
### E52_Time-Span
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/E52_Time-Span`
Description | <p>This class comprises abstract temporal extents, in the sense of Galilean physics, having a beginning, an end and a duration. </p> <p>Time Span has no other semantic connotations. Time-Spans are used to define the temporal extent of instances of E4 Period, E5 Event and any other phenomena valid for a certain time. An E52 Time-Span may be identified by one or more instances of E49 Time Appellation. </p> <p>Since our knowledge of history is imperfect, instances of E52 Time-Span can best be considered as approximations of the actual Time-Spans of temporal entities. The properties of E52 Time-Span are intended to allow these approximations to be expressed precisely. An extreme case of approximation, might, for example, define an E52 Time-Span having unknown beginning, end and duration. Used as a common E52 Time-Span for two events, it would nevertheless define them as being simultaneous, even if nothing else was known. </p> <p>Automatic processing and querying of instances of E52 Time-Span is facilitated if data can be parsed into an E61 Time Primitive.</p> <p>Examples:  1961 From 12-17-1993 to 12-8-1996 14h30 –16h22 4thJuly 1945 9.30 am 1.1.1999 to 2.00 pm 1.1.1999 duration of the Ming Dynasty</p>
In domain of |[crm:P79_beginning_is_qualified_by](http://www.cidoc-crm.org/cidoc-crm/P79_beginning_is_qualified_by) (dp)<br />[crm:P80_end_is_qualified_by](http://www.cidoc-crm.org/cidoc-crm/P80_end_is_qualified_by) (dp)<br />
In range of |[crm:P4_has_time-span](http://www.cidoc-crm.org/cidoc-crm/P4_has_time-span) (op)<br />
### E53_Place
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/E53_Place`
Description | <p>This class comprises extents in space, in particular on the surface of the earth, in the pure sense of physics: independent from temporal phenomena and matter. </p> <p>The instances of E53 Place are usually determined by reference to the position of “immobile” objects such as buildings, cities, mountains, rivers, or dedicated geodetic marks. A Place can be determined by combining a frame of reference and a location with respect to this frame. It may be identified by one or more instances of E44 Place Appellation.</p> <p>It is sometimes argued that instances of E53 Place are best identified by global coordinates or absolute reference systems. However, relative references are often more relevant in the context of cultural documentation and tend to be more precise. In particular, we are often interested in position in relation to large, mobile objects, such as ships. For example, the Place at which Nelson died is known with reference to a large mobile object –H.M.S Victory. A resolution of this Place in terms of absolute coordinates would require knowledge of the movements of the vessel and the precise time of death, either of which may be revised, and the result would lack historical and cultural relevance.</p> <p>Any object can serve as a frame of reference for E53 Place determination. The model foresees the notion of a "section" of an E19 Physical Object as a valid E53 Place determination.</p>
In range of |[crm:P7_took_place_at](http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at) (op)<br />
### E55_Type
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/E55_Type`
Description | <p>This class comprises concepts denoted by terms from thesauri and controlled vocabularies used to characterize and classify instances of CRM classes. Instances of E55 Type represent concepts in contrast to instances of E41 Appellation which are used to name instances of CRM classes. </p> <p>E55 Type is the CRM’s interface to domain specific ontologies and thesauri. These can be represented in the CRM as subclasses of E55 Type, forming hierarchies of terms, i.e. instances of E55 Type linked via P127 has broader term (has narrower term). Such hierarchies may be extended with additional properties.</p>
Sub-classes |[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Role](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Role) (c)<br />
### E7_Activity
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/E7_Activity`
Description | <p>This class comprises actions intentionally carried out by instances of E39 Actor that result in changes of state in the cultural, social, or physical systems documented. </p> <p>This notion includes complex, composite and long-lasting actions such as the building of a settlement or a war, as well as simple, short-lived actions such as the opening of a door.</p> <p>Examples: the Battle of Stalingrad  the Yalta Conference  my birthday celebration 28-6-1995 the writing of “Faust” by Goethe (E65) the formation of the Bauhaus 1919 (E66) calling the place identified by TGN ‘7017998’ ‘Quyunjig’ by the people of Iraq Kira Weber working in glass art from 1984 to 1993 Kira Weber working in oil and pastel painting from 1993</p>
Sub-classes |[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Recording_Role_Activity](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Recording_Role_Activity) (c)<br />[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Work_Public_Release](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Work_Public_Release) (c)<br />
### Thing
Property | Value
--- | ---
URI | `http://www.w3.org/2002/07/owl#Thing`
In domain of |[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#cmtq_id](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#cmtq_id) (dp)<br />[owl:sameAs](http://www.w3.org/2002/07/owl#sameAs) (dp)<br />
### Q1762059
Property | Value
--- | ---
URI | `http://www.wikidata.org/entity/Q1762059`
Description | <p>company that produces films</p>
Super-classes |[crm:E40_Legal_Body](http://www.cidoc-crm.org/cidoc-crm/E40_Legal_Body) (c)<br />
### Q34770
Property | Value
--- | ---
URI | `http://www.wikidata.org/entity/Q34770`
Description | <p>particular system of communication, usually named for the region or peoples that use it</p>
### Q483394
Property | Value
--- | ---
URI | `http://www.wikidata.org/entity/Q483394`
Description | <p>category of creative works based on stylistic, thematic or technical criteria</p>
In range of |[wdt:P136](http://www.wikidata.org/prop/direct/P136) (op)<br />

## Object Properties
[in_the_role_of](#in_the_role_of),
[release_event](#release_event),
[R10_has_member](#R10_has_member),
[R16i_was_initiated_by](#R16i_was_initiated_by),
[R22_created_a_realization_of](#R22_created_a_realization_of),
[R2_is_derivative_of](#R2_is_derivative_of),
[P14_carried_out_by](#P14_carried_out_by),
[P4_has_time-span](#P4_has_time-span),
[P67_refers_to](#P67_refers_to),
[P7_took_place_at](#P7_took_place_at),
[P9_consists_of](#P9_consists_of),
[P136](#P136),
[P407](#P407),
[](in_the_role_of)
### in_the_role_of
Property | Value
--- | ---
URI | `file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#in_the_role_of`
Description | Role associated with the recording role activity.
Domain(s) |[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Recording_Role_Activity](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Recording_Role_Activity) (c)<br />
Range(s) |[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Role](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Role) (c)<br />
[](release_event)
### release_event
Property | Value
--- | ---
URI | `file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#release_event`
Description | Release event of recording work. A recording may be released in multiple locations at different dates.
Super-properties |[crm:P67_refers_to](http://www.cidoc-crm.org/cidoc-crm/P67_refers_to) (op)<br />
Domain(s) |[frbroo:F21_Recording_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F21_Recording_Work) (c)<br />
[](R10_has_member)
### R10_has_member
Property | Value
--- | ---
URI | `http://iflastandards.info/ns/fr/frbr/frbroo/R10_has_member`
Description | This property associates an instance of F15 Complex Work with an instance of F1 Work that forms part of it. The Work becomes complex by the fact that it has other instances of Work as members.  Examples: - Dante’s textual work entitled ‘Divina Commedia’ (F15) R10 has member Dante’s textual work entitled ‘Inferno’ (F15)  - Dante’s textual work entitled ‘Inferno’ (F15) R10 has member The abstract content of the pseudo-old French text of Émile Littré’s translation entitled ‘L’Enfer mis en vieux langage françois et en vers’ [a 19th century translation of Dante’s ‘Inferno’ into old French] published in Paris in 1879 (F14)  - Giovanni Battista Piranesi’s graphic work entitled ‘Carceri’ (F15) R10 has member Giovanni Battista Piranesi’s graphic work entitled ‘Carcere XVI: the pier with chains’ (F15)  - Giovanni Battista Piranesi’s graphic work entitled ‘Carcere XVI: the pier with chains’ (F15) R10 has member The abstract content of Giovanni Battista Piranesi’s graphic work entitled ‘Carcere XVI: the pier with chains: 2nd state’ (F14)
Domain(s) |[frbroo:F15_Complex_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F15_Complex_Work) (c)<br />
Range(s) |[frbroo:F1_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work) (c)<br />
[](R16i_was_initiated_by)
### R16i_was_initiated_by
Property | Value
--- | ---
URI | `http://iflastandards.info/ns/fr/frbr/frbroo/R16i_was_initiated_by`
Description | This property associates the first conception of a work and the work itself that ensued from a given initial idea.  It marks the origin of the causality chain that results in a work’s coming into existence.  Examples:  -Ludwig van Beethoven’s decision to compose a fifth symphony (F27) R16 initiated Ludwig van Beethoven’s Fifth Symphony (F15).  -Pablo Picasso’s acceptance, in 1930, of Ambroise Vollard’s commission for a set of 100 etchings, now known as the ‘Vollard Suite’ (F27) R16 initiated the ‘Vollard Suite’ (F15).  - René Goscinny’s and Albert Uderzo’s decision to collaborate on the comic book entitled ‘Asterix in Britain’ (F27) R16 initiated the comic book entitled ‘Asterix in Britain’ (F15).  -The creative spark that motivated Oscar Wilde, by May 1897, to write a poem inspired by his stay in the Reading prison in 1895-1897 (F27) R16 initiated Oscar Wilde’s poem entitled ‘The ballad of the Reading gaol’ (F15).
Domain(s) |[frbroo:F1_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work) (c)<br />
Range(s) |[frbroo:F27_Work_Conception](http://iflastandards.info/ns/fr/frbr/frbroo/F27_Work_Conception) (c)<br />
[](R22_created_a_realization_of)
### R22_created_a_realization_of
Property | Value
--- | ---
URI | `http://iflastandards.info/ns/fr/frbr/frbroo/R22_created_a_realization_of`
Description | This property associates an instance of F29 Recording Event with the instance of F21 Recording Work it realised.  Examples: - The making of the recording of the third alternate take of the musical work entitled ‘Blue Hawaii’ as performed by Elvis Presley in Hollywood, Calif., Radio Recorders, on March 22nd, 1961 (F29) R22 created a realisation of the concept of the third alternate take of the musical work entitled ‘Blue Hawaii’ as performed by Elvis Presley in Hollywood, Calif.  - The making of the photograph of the three Allied leaders at Yalta in February 1945 (F29) R22 created a realisation of the concept of making a photograph of the three Allied leaders at Yalta in February 1945 (F21).  - The making of the recording of an East Australian humpback whale song in 1994 in the framework of the Oceania Project (F29) R22 created a realisation of Oceania Project’s concept of making a large digital acoustic data archive dedicated to East Australian humpback whale songs (F21).  - Filming Louise Bourgeois at work in the context of the shooting of the documentary movie entitled ‘Louise Bourgeois: The Spider, the Mistress, and the Tangerine’ (F29). R22 created a realisation of the concept of recording Louise Bourgeois’s artistic activity in the documentary movie entitled ‘Louise Bourgeois: The Spider, the Mistress, and the Tangerine’ (F21).
Domain(s) |[frbroo:F29_Recording_Event](http://iflastandards.info/ns/fr/frbr/frbroo/F29_Recording_Event) (c)<br />
Range(s) |[frbroo:F21_Recording_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F21_Recording_Work) (c)<br />
[](R2_is_derivative_of)
### R2_is_derivative_of
Property | Value
--- | ---
URI | `http://iflastandards.info/ns/fr/frbr/frbroo/R2_is_derivative_of`
Description | This property associates an instance of F1 Work which modifies the content of another instance of F1 Work with the latter. The property R2.1 has type of this property allows for specifying the kind of derivation, such as adaptation, summarisation etc.  Examples: - William Schuman’s orchestration of Charles Ives’s ‘Variations on America’ (F15) R2 is derivative of Charles Ives’s ‘Variations on America’ (F15) R2.1 has type orchestration (E55)  - Charles Ives’s musical work entitled ‘Variations on America’ (F15) R2 is derivative of The musical work titled ‘America’ (F15) R2.1 has type variations (E55)  - The musical work entitled ‘America’ (F15) R2 is derivative of The musical work entitled ‘God save the King’ (F15) R2.1 has type same tune with different lyrics (E55)
Domain(s) |[frbroo:F21_Recording_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F21_Recording_Work) (c)<br />
Range(s) |[frbroo:F1_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work) (c)<br />
[](P14_carried_out_by)
### P14_carried_out_by
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P14_carried_out_by`
Description | This property describes the active participation of an E39 Actor in an E7 Activity. It implies causal or legal responsibility. The P14.1 in the role of property of the property allows the nature of an Actor’s participation to be specified.  Examples:   the painting of the Sistine Chapel (E7) carried out by Michaelangelo Buonaroti (E21) in the role of master craftsman (E55)
Domain(s) |[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Recording_Role_Activity](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Recording_Role_Activity) (c)<br />[frbroo:F27_Work_Conception](http://iflastandards.info/ns/fr/frbr/frbroo/F27_Work_Conception) (c)<br />
Range(s) |[crm:E39_Actor](http://www.cidoc-crm.org/cidoc-crm/E39_Actor) (c)<br />
[](P4_has_time-span)
### P4_has_time-span
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P4_has_time-span`
Description | This property describes the temporal confinement of an instance of an E2 Temporal Entity.The related E52 Time-Span is understood as the real Time-Span during which the phenomena were active, which make up the temporal entity instance. It does not convey any other meaning than a positioning on the “time-line” of chronology. The Time-Span in turn is approximated by a set of dates (E61 Time Primitive). A temporal entity can have in reality only one Time-Span, but theremay exist alternative opinions about it, which we would express by assigning multiple Time-Spans. Related temporal entities may share a Time-Span. Time-Spans may have completely unknown dates but other descriptions by which we can infer knowledge.  Examples: the Yalta Conference (E7) has time-span Yalta Conference time-span (E52)
Domain(s) |[frbroo:F29_Recording_Event](http://iflastandards.info/ns/fr/frbr/frbroo/F29_Recording_Event) (c)<br />[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Work_Public_Release](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Work_Public_Release) (c)<br />
Range(s) |[crm:E52_Time-Span](http://www.cidoc-crm.org/cidoc-crm/E52_Time-Span) (c)<br />
[](P67_refers_to)
### P67_refers_to
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P67_refers_to`
Description | This property documents that an E89 Propositional Object makes a statement about an instance of E1 CRM Entity. P67 refers to (is referred to by)has the P67.1 has type link to an instance of E55 Type. This is intended to allow a more detailed description of the type of reference. This differs from P129 is about (is subject of), which describes the primary subject or subjects of the E89 Propositional Object.  Examples: the eBay auction listing of 4 July 2002 (E73) refers tosilver cup 232 (E22)has type item for sale (E55)
[](P7_took_place_at)
### P7_took_place_at
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P7_took_place_at`
Description | This property describes the spatial location of an instance of E4 Period. The related E53 Place should be seen as an approximation of the geographical area within which the phenomena that characterise the period in question occurred. P7took place at (witnessed)does not convey any meaning other than spatial positioning (generally on the surface of the earth). For example, the period “Révolution française” can be said to have taken place in “France”, the “Victorian” period, may be said to have taken place in “Britain” and its colonies, as well as other parts of Europe and north America.  A period can take place at multiple locations.It is a shortcut of the more fully developed path from E4 Period through P161 has spatial projection, E53 Place,P89 falls within (contains)to E53 Place.Describe in words.  Examples: the period “Révolution française” (E4) took place at France (E53)
Domain(s) |[frbroo:F29_Recording_Event](http://iflastandards.info/ns/fr/frbr/frbroo/F29_Recording_Event) (c)<br />[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Work_Public_Release](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Work_Public_Release) (c)<br />
Range(s) |[crm:E53_Place](http://www.cidoc-crm.org/cidoc-crm/E53_Place) (c)<br />
[](P9_consists_of)
### P9_consists_of
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P9_consists_of`
Description | This property associates an instance of E4 Period with another instance of E4 Period that is defined by a subset of the phenomena that define the former. Therefore the spacetime volume of the latter must fall within the spacetime volume of the former.  Examples: Cretan Bronze Age (E4) consists of Middle Minoan (E4)
Domain(s) |[frbroo:F29_Recording_Event](http://iflastandards.info/ns/fr/frbr/frbroo/F29_Recording_Event) (c)<br />
Range(s) |[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Recording_Role_Activity](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Recording_Role_Activity) (c)<br />
[](P136)
### P136
Property | Value
--- | ---
URI | `http://www.wikidata.org/prop/direct/P136`
Description | creative work's genre or an artist's field of work (P101). Use main subject (P921) to relate creative works to their topic
Domain(s) |[frbroo:F21_Recording_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F21_Recording_Work) (c)<br />
Range(s) |[wd:Q483394](http://www.wikidata.org/entity/Q483394) (c)<br />
[](P407)
### P407
Property | Value
--- | ---
URI | `http://www.wikidata.org/prop/direct/P407`
Description | language associated with this creative work (such as books, shows, songs, or websites) or a name (for persons use "native language" (P103) and "languages spoken, written or signed" (P1412))
Domain(s) |[frbroo:F21_Recording_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F21_Recording_Work) (c)<br />
Range(s) |[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Language](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Language) (c)<br />

## Datatype Properties
[cmtq_id](#cmtq_id),
[episode_number](#episode_number),
[synopsis](#synopsis),
[budget](#budget),
[P79_beginning_is_qualified_by](#P79_beginning_is_qualified_by),
[P80_end_is_qualified_by](#P80_end_is_qualified_by),
[sameAs](#sameAs),
[familyName](#familyName),
[givenName](#givenName),
[name](#name),
[](cmtq_id)
### cmtq_id
Property | Value
--- | ---
URI | `file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#cmtq_id`
Description | Cinematheque identifier of a concept.
Domain(s) |[owl:Thing](http://www.w3.org/2002/07/owl#Thing) (c)<br />
Range(s) |[xsd:positiveInteger](http://www.w3.org/2001/XMLSchema#positiveInteger) (c)<br />
[](episode_number)
### episode_number
Property | Value
--- | ---
URI | `file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#episode_number`
Domain(s) |[file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Episode](file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#Episode) (c)<br />
Range(s) |[xsd:integer](http://www.w3.org/2001/XMLSchema#integer) (c)<br />
[](synopsis)
### synopsis
Property | Value
--- | ---
URI | `file:///home/kolam/git/cmtqo/cmtqo/cmtqo.ttl#synopsis`
Domain(s) |[frbroo:F21_Recording_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F21_Recording_Work) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](budget)
### budget
Property | Value
--- | ---
URI | `http://dbpedia.org/ontology/budget`
Domain(s) |[frbroo:F21_Recording_Work](http://iflastandards.info/ns/fr/frbr/frbroo/F21_Recording_Work) (c)<br />
Range(s) |[xsd:double](http://www.w3.org/2001/XMLSchema#double) (c)<br />
[](P79_beginning_is_qualified_by)
### P79_beginning_is_qualified_by
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P79_beginning_is_qualified_by`
Description | This property qualifies the beginning of an E52 Time-Span in some way. The nature of the qualificationmay be certainty, precision, source etc.  Examples: the time-span of the Holocene (E52) beginning is qualified by approximately (E62
Domain(s) |[crm:E52_Time-Span](http://www.cidoc-crm.org/cidoc-crm/E52_Time-Span) (c)<br />
Range(s) |[xsd:dateTime](http://www.w3.org/2001/XMLSchema#dateTime) (c)<br />
[](P80_end_is_qualified_by)
### P80_end_is_qualified_by
Property | Value
--- | ---
URI | `http://www.cidoc-crm.org/cidoc-crm/P80_end_is_qualified_by`
Description | This property qualifies the end of an E52 Time-Span in some way. The nature of the qualification may be certainty, precision, source etc.  Examples: the time-span of the Holocene (E52) end is qualified by approximately (E62)
Domain(s) |[crm:E52_Time-Span](http://www.cidoc-crm.org/cidoc-crm/E52_Time-Span) (c)<br />
Range(s) |[xsd:dateTime](http://www.w3.org/2001/XMLSchema#dateTime) (c)<br />
[](sameAs)
### sameAs
Property | Value
--- | ---
URI | `http://www.w3.org/2002/07/owl#sameAs`
Domain(s) |[owl:Thing](http://www.w3.org/2002/07/owl#Thing) (c)<br />
Range(s) |[xsd:anyURI](http://www.w3.org/2001/XMLSchema#anyURI) (c)<br />
[](familyName)
### familyName
Property | Value
--- | ---
URI | `http://xmlns.com/foaf/0.1/familyName`
Domain(s) |[crm:E21_Person](http://www.cidoc-crm.org/cidoc-crm/E21_Person) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](givenName)
### givenName
Property | Value
--- | ---
URI | `http://xmlns.com/foaf/0.1/givenName`
Domain(s) |[crm:E21_Person](http://www.cidoc-crm.org/cidoc-crm/E21_Person) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />
[](name)
### name
Property | Value
--- | ---
URI | `http://xmlns.com/foaf/0.1/name`
Domain(s) |[crm:E21_Person](http://www.cidoc-crm.org/cidoc-crm/E21_Person) (c)<br />
Range(s) |[xsd:string](http://www.w3.org/2001/XMLSchema#string) (c)<br />

## Named Individuals
## Namespaces
* **crm**
  * `http://www.cidoc-crm.org/cidoc-crm/`
* **dbo**
  * `http://dbpedia.org/ontology/`
* **dcterms**
  * `http://purl.org/dc/terms/`
* **foaf**
  * `http://xmlns.com/foaf/0.1/`
* **frbroo**
  * `http://iflastandards.info/ns/fr/frbr/frbroo/`
* **owl**
  * `http://www.w3.org/2002/07/owl#`
* **prov**
  * `http://www.w3.org/ns/prov#`
* **rdf**
  * `http://www.w3.org/1999/02/22-rdf-syntax-ns#`
* **rdfs**
  * `http://www.w3.org/2000/01/rdf-schema#`
* **sd**
  * `https://w3id.org/okn/o/sd#`
* **sdo**
  * `https://schema.org/`
* **skos**
  * `http://www.w3.org/2004/02/skos/core#`
* **wd**
  * `http://www.wikidata.org/entity/`
* **wdt**
  * `http://www.wikidata.org/prop/direct/`
* **xsd**
  * `http://www.w3.org/2001/XMLSchema#`

## Legend
* Classes: c
* Object Properties: op
* Functional Properties: fp
* Data Properties: dp
* Annotation Properties: dp
* Properties: p
* Named Individuals: ni