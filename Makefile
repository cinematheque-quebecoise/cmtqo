# Date of data exportation.
ONTOLOGY_FILE_OWL=cmtqo/cmtqo.owl
ONTOLOGY_FILE_TTL=cmtqo/cmtqo.ttl
ONTOLOGY_FILE_MD=cmtqo/cmtqo.md
ONTOLOGY_FILE_HTML=cmtqo/cmtqo.html
METADATA_FILE=config/metadata.yml
# Executable name
EXEC := $(shell grep "name:\s*" package.yaml | sed "s/name:\s*\(.*\)\s*/\1/")-exe
GITLAB_PROJECT_ID=18249681
VERSION := $(shell grep "version:\s*" package.yaml | sed "s/version:\s*\(.*\)\s*/\1/")
DESTDIR=./cmtqo
GITLAB_TOKEN := $(shell cat .gitlab-token)

pre-commit:
	pre-commit run --all-files

run: run-cmtqo-exe run-pylode

run-cmtqo-exe: ${ONTOLOGY_FILE_TTL}

${ONTOLOGY_FILE_TTL}: ${ONTOLOGY_FILE_OWL} ${METADATA_FILE}
	$(EXEC) ${ONTOLOGY_FILE_OWL} ${METADATA_FILE}

run-pylode: ${ONTOLOGY_FILE_MD} ${ONTOLOGY_FILE_HTML}

${ONTOLOGY_FILE_MD}: ${ONTOLOGY_FILE_TTL}
	pylode -i ${ONTOLOGY_FILE_TTL} -o ${ONTOLOGY_FILE_MD} -f md

${ONTOLOGY_FILE_HTML}: ${ONTOLOGY_FILE_TTL}
	pylode -i ${ONTOLOGY_FILE_TTL} -o ${ONTOLOGY_FILE_HTML} -f html

# If release tag does not exist on Gitlab server, it returns a 403 Forbidden HTTP code.
# @param token - Private Gitlab token
# $ make release token=<YOURTOKEN>
release: $(DESTDIR)
	./create-release.sh \
		"cmtqo v$(VERSION)" \
		"v$(VERSION)" \
		$(GITLAB_PROJECT_ID) \
		"New release of cmtqo v$(VERSION)" \
		$(GITLAB_TOKEN) \
		$<
