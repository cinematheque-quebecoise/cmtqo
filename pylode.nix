{ pkgs }:

pkgs.python3Packages.buildPythonApplication rec {
  pname = "pyLODE";
  version = "2.8.3";

  src = pkgs.fetchFromGitHub {
    owner = "RDFLib";
    repo = pname;
    # ref = "refs/head/master";
    rev = "c0c5c3812b0ce341e98a39642b28904227b66565";
    # rev = version;
    sha256 = "1cyknrin91jh529a33hvz55v3i49flyxv4vw036z5rm1iwp4jz9f";
  };

  # Waiting for argparse to be removed in requirements.txt from upstream package
  postPatch = ''
    substituteInPlace requirements.txt --replace "argparse" ""
  '';

  propagatedBuildInputs = with pkgs.python3Packages; [
    rdflib
    rdflib-jsonld
    requests
    dateutil
    markdown
    jinja2
    gunicorn
    pytest
    falcon
    isodate
    six
  ];

  # Import errors in pylode tests
  doCheck = false;
}
