let
  pkgs = import ./nixpkgs.nix;
  inherit (pkgs) haskellPackages;

  project = import ./release.nix;
  eddy = pkgs.callPackage ./eddy.nix {};
  pylode = pkgs.callPackage ./pylode.nix {};
in

  pkgs.mkShell {
    buildInputs = project.env.nativeBuildInputs ++ [
      project
      eddy
      pylode
      haskellPackages.cabal-install
      pkgs.curl
      pkgs.jq
      pkgs.python38Packages.pre-commit
    ];

    shellHook = ''
      export SSL_CERT_FILE=/etc/ssl/certs/ca-bundle.crt
      pre-commit install
    '';
  }
